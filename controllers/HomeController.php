<?php

class HomeController extends Controller
{
    public function index()
    {
        self::view('home');
    }
    public function projeozellikleri()
    {
        self::view('projeozellikleri');
    }
    public function daireplanlari()
    {
        self::view('daireplanlari');
    }
    public function basinda()
    {
        self::view('basinda');
    }
    public function lokasyon()
    {
        self::view('lokasyon');
    }
    public function iletisim()
    {
        self::view('iletisim');
    }
    public function galeri()
    {
        self::view('galeri');
    }
    public function yasamsecenekleri()
    {
        self::view('yasam_secenekleri');
    }
    public function formsuccessful()
    {
        if (!isset($_SESSION['formid'])) {
            self::view('home');
        }
        unset($_SESSION['formid']);
        self::view('form_successful');
    }
    public function gorsel360()
    {
        self::view('360');
    }
    public function sendform()
    {
        $contentType = isset($_SERVER["CONTENT_TYPE"]) ? trim($_SERVER["CONTENT_TYPE"]) : '';
        $arr = array('success' => true, 'result' => array(), 'errorcount' => 0);

        if ($contentType === "application/json") {
            $content = trim(file_get_contents("php://input"));
            /** Gönderilecek datalar */
            $data = json_decode($content, true)['data'];
            $object = new stdClass();
            $object->ADSOYAD = $data['name'];
            $object->EPOSTA = $data['email'];
            $object->TEL = $data['phone'];
            $object->KAYNAK = "Web";
            $object->REFERER_URL = '';
            $object->PID = 1;
            $object->REF_NO = "J7lkrr9RUJ";
            try {
                if ($data['name'] === null || strlen($data['name']) < 5) {
                    $arr['result']['name'] = "invalid";
                    $arr['success'] = false;
                    $arr['errorcount'] += 1;
                }
                if ($data['phone'] === null || strlen($data['phone']) < 10) {
                    $arr['result']['phone'] = "invalid";
                    $arr['success'] = false;
                    $arr['errorcount'] += 1;
                }
                if ($data['phone'] !== null) {
                    $data['phone'] = preg_replace('/[^0-9]/', '', $data['phone']);
                }
                if ($data['email'] === null || !filter_var($data['email'], FILTER_VALIDATE_EMAIL)) {
                    $arr['result']['email'] = "invalid";
                    $arr['success'] = false;
                    $arr['errorcount'] += 1;
                }
                if ($data['isApprove'] === null) {
                    $arr['result']['isapprove'] = "invalid";
                    $arr['success'] = false;
                    $arr['errorcount'] += 1;
                }

                $object->MESAJ = sprintf("Satın Alım amacı = Yatırım : %s, Oturum : %s ", $data['options']['investment'] ? 'Evet' : 'Hayır', $data['options']['sitting'] ? 'Evet' : 'Hayır');

                if ($arr['success'] == true) {
                    try {
                        $client = new SoapClient("http://asafwebpost.satnetcrm.com/info_form.asmx?WSDL");
                        $result = $client->__soapCall("WebForm", array($object));
                    } catch (\Throwable $th) {
                        $arr['success'] = false;
                        $arr['error_code'] = 500;
                    }
                    if ($result->WebFormResult == true) {
                        $arr['success'] = $result->WebFormResult;
                        $arr['formid'] = md5(uniqid(mt_rand(), true));
                        $_SESSION['formid'] = $arr['formid'];
                    }
                }
            } catch (Exception $th) {
                $arr['success'] = false;
                $arr['error'] = true;
            }
        }
        echo json_encode($arr, true);
    }
}
