<?php

/** Genel Rotalar */
Router::get('', 'HomeController', 'index');
Router::get('/tesekkurler', 'HomeController', 'index');
Router::get('/daire-planlari', 'HomeController', 'daireplanlari');
Router::get('/proje-ozellikleri', 'HomeController', 'projeozellikleri');
Router::get('/basinda', 'HomeController', 'basinda');
Router::get('/lokasyon', 'HomeController', 'lokasyon');
Router::get('/iletisim', 'HomeController', 'iletisim');
Router::get('/galeri', 'HomeController', 'galeri');
Router::get('/360', 'HomeController', 'gorsel360');
Router::get('/yasam-secenekleri', 'HomeController', 'yasamsecenekleri');
Router::get('/form-successful', 'HomeController', 'formsuccessful');
Router::post('/form/send', 'HomeController', 'sendform');

