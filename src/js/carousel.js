export default class Carousel {
    constructor(options) {
        const ques = (name) => document.querySelector(name);
        this.options = options;
        this.name = this.options.name;
        this.context = ques(this.options.context);
        if (!this.context) return;
        this.items = this.context.querySelectorAll(options.items);
        this.currentIndex = options.currentIndex || 0;
        this.leftButton = options.left && ques(options.left);
        this.rightButton = options.right && ques(options.right);
        this.thumbs = options.thumbs && ques(options.thumbs);
        this.columnSize = Math.abs(options.columnSize);
        this.mediaquery = options.mediaquery;
        this.pageSchema = [];
        this.setup();
        this.calcPage();
        this.limit();
        this.slide();
        this.setThumb(this.currentIndex);
    }

    autoSlide() {
        if (this.options.isAuto && this.options.isAnimate) {
            this.options.timeout = setTimeout(() => {
                this.currentIndex = this.currentIndex >= this.pageSchema.length - 1 ? 0 : this.currentIndex += 1;
                this.slide();
                this.autoSlide();
                this.limit();
            }, this.options.timer || 7000);
        }
    }
    startAutoSlide() {
        this.options.timeout && clearTimeout(this.options.timeout);
        this.autoSlide();
    }

    slide() {
        const size = this.context.dimension;
        this.currentPosition = -this.items[0].dimension.width * this.currentIndex;
        this.animation(this.currentPosition);
        this.setThumb(this.currentIndex);
    }

    animation(position, param = {}) {
        if (!this.options.showHideItem) {
            this.context.css = { 'transform': `translateX(${position}px)` };
            this.context.css = param;
        }
    }

    showHideItemClear() {
        const self = this;
        if (self.options.showHideItem) {
            self.items.forEach((item, index) => {
                item.style.display = self.options.defaultDisplay;
            });
        }
    }

    showHideItem() {
        const self = this;
        if (self.options.showHideItem) {
            self.items.forEach((item, index) => {
                item.style.display = self.currentIndex == index ? (self.options.showDefaultDisplay || 'block') : 'none';
            });
        }
    }

    limit() {
        const hidden = { opacity: 0, pointerEvents: 'none' };
        if (this.leftButton) {
            const isLeft = this.currentIndex <= 0;
            this.leftButton.css = isLeft ? hidden : { opacity: 1, pointerEvents: 'inherit' };
            this.currentIndex = isLeft ? 0 : this.currentIndex;
        }
        if (this.rightButton) {
            const isRight = this.currentIndex >= this.pageSchema.length - 1;
            this.rightButton.css = isRight ? hidden : { opacity: 1, pointerEvents: 'inherit' };
            this.currentIndex = isRight ? this.items.length - 1 : this.currentIndex;
        }
    }

    next() {
        this.currentIndex++;
        this.start();
    }

    prev() {
        this.currentIndex--;
        this.start();
    }

    start() {
        this.limit();
        this.slide();
        this.showHideItem();
    }

    touchstart(e) {
        this.setPosition(e, 'start');
    }
    resize(e) {
        this.options.dimension.width = e.target.innerWidth;
        this.calcPage();
        this.startAutoSlide();
        this.slide();
    }

    setPosition(ev, name) {
        if (ev.changedTouches && ev.changedTouches.length) {
            this.touches[name] = ev.changedTouches[0].clientX;
        }
    }

    touchend(e) {
        this.setPosition(e, 'end');
        if (Math.abs(this.touches.diff) > (this.context.dimension.width * .2)) {
            if (this.touches.end > this.touches.start) {
                this.currentIndex--;
            } else {
                this.currentIndex++;
            }
        }
        this.context.style = '';
        this.touches.ismove = false;
        this.start();
    }

    touchmove(e) {
        this.touches.ismove = true;
        this.setPosition(e, 'move');
        this.touches.diff = this.touches.move - this.touches.start;
        this.animation(this.currentPosition + this.touches.diff * .2, { 'transition': 'inherit' });
    }

    calcPage() {
        if (this.columnSize <= 0 || !this.columnSize === undefined) return;
        this.pageSchema = [];
        let count = this.items.length;
        let thumbType = "span";
        if (this.thumbs) {
            this.thumbs.innerHTML = '';
            thumbType = this.thumbs.getAttribute('data-thumb-type') || 'span';
        }
        let index = 0;
        while (count > 0) {
            this.pageSchema.push(count >= this.columnSize ? this.columnSize : 1);
            count -= this.columnSize;
            if (this.thumbs) {
                const thumb = document.createElement(thumbType);
                this.thumbs.appendChild(thumb);
                thumb.e = ['click', (e) => {
                    this.onClickThumb(e);
                    this.startAutoSlide();
                }];
            }
            index++;
        }
    }

    onClickThumb(e) {
        this.currentIndex = e.target.index;
        this.slide();
    }

    setThumb(index) {
        if (!this.thumbs || this.thumbs.children.length === 0) return;
        this.thumbs.children.each((item, i) => {
            if (index === i) {
                item.classList.add('active');
            } else {
                item.classList.remove('active');
            }
        });
    }
    mediaResize() {
        const sizes = this.mediaquery;
        const ordered = {};
        Object.keys(sizes).sort().forEach(function(key) {
            ordered[key] = sizes[key];
        });
        const self = this;
        const options = this.options;

        let isFind = false;
        Object.keys(ordered).forEach(item => {
            const size = sizes[item];
            item = parseInt(item);
            if (self.context.dimension.width <= item && item != 0) {
                isFind = true;
                self.columnSize = size;
                self.options.showHideItem && self.showHideItem();
                self.context.removeAttribute('data-carousel-fit');
                return;
            }
        });
        if (!isFind) {
            self.columnSize = ordered[0] || 1;
            if (self.options.showHideItem) {
                self.showHideItemClear();
                self.context.setAttribute('data-carousel-fit', '');
            }
        }
    }
    media() {
        window.addEventListener('resize', () => this.mediaResize(), false);
        this.mediaResize();
    }

    setup() {

        this.options.dimension = {
            width: window.width
        }

        if (this.items.length && this.columnSize > 0) {
            for (let i = 0; i < this.items.length; i++) {
                this.items[i].style.minWidth = `${100 / this.columnSize}%`;
            }
        }

        if (this.leftButton) {
            this.leftButton.e = ['click', (e) => this.prev(e)];
        }
        if (this.rightButton) {
            this.rightButton.e = ['click', (e) => this.next(e)];
        }
        if (this.mediaquery) {
            this.media();
        }

        this.context.e = ['mouseover', () => (this.options.isAnimate = false)];
        this.context.e = ['mouseout', () => {
            this.options.isAnimate = true;
            this.startAutoSlide();
        }];

        this.options.isAnimate = this.options.isAnimate === undefined ? true : this.options.isAnimate;
        this.options.isAuto = this.options.isAuto === undefined ? true : this.options.isAuto;

        if (this.isMobile()) {
            this.context.touchstart = (e) => this.touchstart(e);
            this.context.touchmove = (e) => this.touchmove(e);
            this.context.touchend = (e) => this.touchend(e);
            this.touches = {};
        }
        window.addEventListener('resize', (e) => this.resize(e), false);
        if (this.options.isAuto) {
            this.startAutoSlide();
        }
    }

    isMobile() {
        var check = false;
        (function(a) { if (/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(a) || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0, 4))) check = true; })(navigator.userAgent || navigator.vendor || window.opera);
        return check;
    }
}