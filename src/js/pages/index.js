import Carousel from '../carousel';

export default class Main {
    constructor() {
        new Carousel({
            name: 'common-carousel',
            context: '#carousel-container__context',
            items: '.carousel-container__context__item',
            left: '.carousel-container__arrow--left',
            right: '.carousel-container__arrow--right',
            columnSize: 1,
        });
        new Carousel({
            name: 'basaksehir-carousel',
            context: '#basaksehir-story-carorusel-context',
            items: '.carousel-standard--item',
            thumbs: '.carousel-standard--control',
            columnSize: 1
        });
        new Carousel({
            name: 'istanbul-story__props-carousel',
            context: '.istanbul-story__props ul',
            items: 'li.story-prop',
            left: '.istanbul-story__arrow--left',
            right: '.istanbul-story__arrow--right',
            showHideItem: true,
            defaultDisplay: '',
            showDefaultDisplay: 'block',
            columnSize: 0,
            mediaquery: {
                0: 0,
                768: 1
            },
        });

        new Carousel({
            context: '.istanbul-way-story__mobile-context',
            items: '.istanbul-way-story__mobile-item',
            showHideItem: true,
            defaultDisplay: '',
            showDefaultDisplay: 'block',
            columnSize: 1,
            left: '.istanbul-way-story__mobile__arrow--left',
            right: '.istanbul-way-story__mobile__arrow--right',
        });

        const requestFormButton = document.querySelector('#mobile-request-form');
        const requestForm = document.querySelector('.request-form-mobile');
        const requestFormClose = document.querySelector('.request-form-close button');

        requestFormButton.e = ['click', (e) => {
            requestForm.visible = true;
            document.body.overflow = 'hidden';
        }];
        requestFormClose.e = ['click', (e) => {
            requestForm.visible = false;
            document.body.overflow = 'inherit';
        }];

    }
}