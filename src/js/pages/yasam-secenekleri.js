import Carousel from '../carousel';
export default class YasamSecenekleri {
  constructor() {
    this.init();
  }
  init() {

    new Carousel({
      context: '#life-options-items',
      items: 'img',
      thumbs: '#carousel-tumbs-moda',
      columnSize: 1,
    });

    new Carousel({
      context: '#life-options-items-bahce',
      items: 'img',
      thumbs: '#carousel-tumbs-bahce',
      columnSize: 1,
    });

    new Carousel({
      context: '#life-options-items-grand',
      items: 'img',
      thumbs: '#carousel-tumbs-grand',
      columnSize: 1,
    });


  }
}