import Carousel from '../carousel';
export default class ProjeOzellikleri{
    constructor(){//
        new Carousel({
            context: '#life-options-items-bahce',
            items: 'img',
            thumbs: '#carousel-tumbs-bahce',
            columnSize: 1,
          });
          new Carousel({
            context: '#options-items-ozellik2',
            items: 'img',
            thumbs: '#carousel-tumbs-ozellik2',
            columnSize: 1,
          });
          new Carousel({
            context: '.istanbul-story__props ul',
            items: 'li.story-prop',
            left: '.istanbul-story__arrow--left',
            right: '.istanbul-story__arrow--right',
            showHideItem: true,
            defaultDisplay: '',
            showDefaultDisplay: 'block',
            columnSize: 0,
            mediaquery: {
              0: 0,
              768: 1
            },
          });
      
    }
}