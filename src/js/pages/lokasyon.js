import Carousel from '../carousel';
export default class Lokasyon {
  constructor() {
    new Carousel({
      context: '.istanbul-way-story__mobile-context',
      items: '.istanbul-way-story__mobile-item',
      showHideItem: true,
      defaultDisplay: '',
      showDefaultDisplay: 'block',
      columnSize: 1,
      left: '.istanbul-way-story__mobile__arrow--left',
      right: '.istanbul-way-story__mobile__arrow--right',
    });
  }
}