export default function Popup() {

    const list = document.querySelectorAll('[open-popup]');
    if (list.length) {
        list.forEach(item => {

            const attr = item.getAttribute('open-popup');
            if (attr) {
                const el = document.querySelector(attr);
                item.addEventListener('click', function () {
                    el.style.display = 'block';
                });
                if (el) {
                    const close = el.querySelector('[close-popup]');
                    console.log(close.isevent);
                    if (!close.isevent) {
                        close.isevent = true;
                        close.addEventListener('click', function () {
                            el.parentNode.removeChild(el);
                        });
                    }
                }
            }
        });
    }

    const closeModal = document.querySelector('[close-modal]');
    if(closeModal){
        closeModal.addEventListener('click',function(){
            const el = document.querySelector(closeModal.getAttribute('close-modal'));
            if(el){
                el.classList.remove('active');
            }
        });
    }
}
