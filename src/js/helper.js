export default class Helper {
  static loader(elements) {
    for (let i = 0; i < elements.length; i++) {
      Helper.image(elements[i]);
    }
  }
  static image(el) {
    const img = new Image();
    const src = el.getAttribute('data-src');
    el.src = src;
    // img.onload = function () {
    //     el.src = src;
    // }
    // img.onerror = function(ev,src,numb,cl,err){
    //   console.log(ev,src,numb,cl,err);
    // }
    // img.src = src;
  }
}