export default function FormApplication() {
  const form = document.querySelectorAll(".form-application");
  if (form.length > 0) {
    form.forEach((item) => new setup(item));
  }

  function setup(_form) {
    _form.addEventListener("submit", _submit, false);
  }

  function showError(form, errs) {
    let status = form.querySelector(".form-status");
    status.innerHTML = "<label></label>";
    for (let key in errs) {
      let el = document.querySelector(`[name=rf__${key}]`);
      if (el) {
        let p = document.createElement("p");
        p.innerText = `* ${el.getAttribute("data-error")}`;
        status.firstChild.appendChild(p);
      }
    }
  }

  /** Form gönderim işlemi */
  function _submit(e) {
    /** Mevcut form işlemini iptal et */
    e.preventDefault();
    /** Nesne seçmenin kısa yolu  */
    const query = (item) => e.target.querySelector(`[name=${item}]`);
    /** GÖnderilme işlemini hata kontrolü içinde yönetiyoruz */
    try {
      /** Gönderilecek datalar */
      const _data = {
        name: query("rf__name").value,
        email: query("rf__email").value,
        phone: query("rf__phone").value,
        options: {
          investment: query("rf__buy_investment").checked,
          sitting: query("rf__buy_sitting").checked,
        },
        isApprove: query("rf__confirm_info").checked,
      };
      /** Bilgilendirme yazısı onay */
      if (!_data.isApprove) {
        return false;
      }
      /** Gönderilme işlemi öncesinde formu kilitle */
      e.target.setAttribute("data-locked", "");
      e.target.removeAttribute("data-error");
      /** Gönderilmeyi başlat */
      (async () => {
        /** Gönder */
        const _request = await fetch("/form/send", {
          method: "POST",
          mode: "same-origin",
          credentials: "same-origin",
          headers: {
            "Content-Type": "application/json",
          },
          body: JSON.stringify({ data: _data }),
        });
        /** Cevap bekleniyor */
        const _response = await _request.json();
        if (_response.success) {
          if (_response.success == "true") {
            /** Olumluysa yönlendir */
            e.target.setAttribute("data-error", "");
            location.href = `/tesekkurler?id=${_response.formid}`;
          } else {
            e.target.removeAttribute("data-locked");
            e.target.setAttribute("data-error", "");
            showError(e.target, 'Beklenmedik bir hata oluştu');
          }
        } else {
          /** Olumlu değilse formu aktif et */
          e.target.removeAttribute("data-locked");
          e.target.setAttribute("data-error", "");
          showError(e.target, _response.result);
          /** Uyarı göster */
        }
      })();
    } catch (error) {
      /** Olumlu değilse formu aktif et */
      e.target.removeAttribute("data-locked");
    }
  }
}
