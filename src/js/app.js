require('../sass/all.scss');
import './prototype';
import YasamSecenekleri from './pages/yasam-secenekleri';
import Main from './pages/index';
import Lokasyon from './pages/lokasyon';
import Planlar from './pages/planlar';
import Galeri from './pages/galeri';
import Helper from './helper';
import AOS from 'aos';
import ProjeOzellikleri from './pages/projeozellikleri';
import FormApplication from './formapplication';
import OpenPopup from './popup';

const Pages = [
  {
    name: 'main',
    pages: Main
  },
  {
    name: 'yasam-secenekleri',
    pages: YasamSecenekleri
  },
  {
    name: 'lokasyon',
    pages: Lokasyon
  },
  {
    name: 'galeri',
    pages: Galeri
  },
  {
    name: 'planlar',
    pages: Planlar
  },
  {
    name: 'projeozellikleri',
    pages: ProjeOzellikleri
  }
];

const imgs = document.querySelectorAll('img[data-src]');
Helper.loader(imgs);
window.onload = function () {
  AOS.init();
  FormApplication();
  OpenPopup();
}

const mobileButton = document.querySelector('.mobile-menu button');
const menu = document.querySelector('.header-menu__bottom--context');
mobileButton.e = ['click', (e) => {
  menu.classList.add('active');
}];

const currentPage = Pages.filter(item => item.name == window.page);
currentPage.length && currentPage[0].pages && new currentPage[0].pages();